﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("---------------------------");
            Console.WriteLine("TA - Proxy Checker");
            Console.WriteLine(" ");
            var proxy_url = System.Configuration.ConfigurationManager.AppSettings["proxy"];

            var endpoint = System.Configuration.ConfigurationManager.AppSettings["endpoint"]; ;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(endpoint);
            if (!String.IsNullOrEmpty(proxy_url))
            {
                Console.Write("Proxy: {0}", proxy_url);

                var user = System.Configuration.ConfigurationManager.AppSettings["user"];
                var password = System.Configuration.ConfigurationManager.AppSettings["password"];

                WebProxy myproxy = new WebProxy(proxy_url, false);
                if (!String.IsNullOrEmpty(user))
                {
                    Console.WriteLine(" ");
                    Console.WriteLine("Proxy User: \"{0}\"", user);
                    
                    myproxy.Credentials = new System.Net.NetworkCredential(user, password);
                }
                else
                {
                    Console.Write(" -> (Sin Credenciales)");
                }
                request.Proxy = myproxy;

                Console.WriteLine(" ");

            }
            else
            {
                Console.WriteLine("Proxy: No configurado");
            }
            Console.WriteLine("endpoint: {0}", endpoint);


            Console.WriteLine("---------------------------");
            Console.WriteLine(" ");

            try
            {
                request.Method = "GET";
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                using (Stream responseStream = response.GetResponseStream())
                {

                    var reader = new StreamReader(response.GetResponseStream());
                    string result = reader.ReadToEnd();

                    Console.WriteLine("RESULTADO: ");
                    Console.WriteLine(result);
                }


            }
            catch (Exception ex)
            {
                Console.WriteLine("ERROR: ");
                Console.WriteLine(ex.Message);
            }

            Console.ReadLine();
        }
    }
}
